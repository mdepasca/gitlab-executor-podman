import argparse
import logging
import multiprocessing
import pathlib
import shutil
import socket
import stat
import subprocess

import gitlab
import tomlkit


log = logging.getLogger(__name__)


def register_podman_executor(install_prefix, gitlab_url, auth_token,
                             registration_token, config_file, mode, paused):
    bindir = (install_prefix/'bin').expanduser()
    hostname = socket.gethostname().split('.')[0]
    homedir = pathlib.Path('~').expanduser()
    with open(config_file) as f:
        cfg = tomlkit.parse(f.read())
        if cfg['concurrent'] == 1:
            resources = 'reserved'
        else:
            resources = 'shared'

    runner_name = f'{hostname}-podman'
    limit = max(1, int(multiprocessing.cpu_count() // 4) - 1)
    tags = ['podman', 'container', mode, hostname, resources]
    params = {
        'config-exec': bindir/'ci-podman-config.sh',
        'config-exec-timeout': 3 * 60,
        'prepare-exec': bindir/'ci-podman-prepare.sh',
        'prepare-exec-timeout': 30 * 60,
        'run-exec': bindir/'ci-podman-run.sh',
        'cleanup-exec': bindir/'ci-podman-cleanup.sh',
        'cleanup-exec-timeout': 5 * 60,
        'graceful-kill-timeout': 1 * 60,
        'force-kill-timeout': 3 * 60,
    }
    variables = {
        'CI_RUNNER_BUILDS_DIR': homedir/'builds',
        'CI_RUNNER_CACHE_DIR': homedir/'cache',
        'CI_CACHE_DIR': '/ci/cache',
        'CI_DEFAULT_JOB_IMAGE': 'alpine:latest',
        'CI_IMAGE_PULL_POLICY': 'always',
        'CI_HELPER_IMAGE': 'gitlab/gitlab-runner:alpine',
        'CI_HELPER_ENTRYPOINT': '/usr/bin/dumb-init',
        'CI_HELPER_IMAGE_PULL_POLICY': 'missing',
    }

    gl = gitlab.Gitlab(gitlab_url, auth_token)

    runners = gl.runners.all(type='instance_type', all=True)
    runners = list(filter(lambda r: r.description == runner_name, runners))
    if len(runners) > 1:
        raise SystemError(f'multiple runners found with name {runner_name}')
    elif len(runners) == 1:
        runner = gl.runners.get(runners[0].id)
    else:
        runner = None

    if runner is None:
        log.info(f'registering new runner {runner_name}')
        cmd = ['gitlab-runner', 'register', '--non-interactive',
               '--config', config_file, '--url', gitlab_url,
               '--registration-token', registration_token,
               '--executor', 'custom', '--name', runner_name,
               '--tag-list', ','.join(tags), '--limit', str(limit),
               '--builds-dir', '/ci/builds', '--cache-dir', '/ci/cache']
        if paused:
            cmd += ['--paused']
        for k, v in params.items():
            cmd.extend((f'--custom-{k}', str(v)))
        for k, v in variables.items():
            cmd.extend(('--env', f'{k}={v}'))
        log.debug(f'running command: {" ".join(map(str, cmd))}')
        subprocess.run(cmd, check=True)
    else:
        with open(config_file, 'r+') as f:
            cfg = tomlkit.parse(f.read())
            fltr = lambda r: r['name'] == runner_name
            runnercfg = list(filter(fltr, cfg['runners']))[0]
            runnercfg['limit'] = limit
            for k, v in params.items():
                if not isinstance(v, (str, int)):
                    v = str(v)
                runnercfg['custom'][k.replace('-', '_')] = v
            runnercfg['environment'] = [f'{k}={v}' for k, v in variables.items()]
            f.seek(0)
            f.write(tomlkit.dumps(cfg, sort_keys=True))
            f.truncate()

        runner.active = not paused
        runner.tag_list = tags
        runner.save()


def install_podman_executor(install_prefix):
    here = pathlib.Path(__file__).parent
    bindir = install_prefix/'bin'
    bindir.mkdir(parents=True, exist_ok=True)
    for script in (here/'scripts').glob('*.sh'):
        shutil.copy(script, bindir)


def secret(path_or_secret):
    path_or_secret = pathlib.Path(path_or_secret).expanduser()
    if not path_or_secret.is_file():
        return str(path_or_secret).strip()
    else:
        mode = path_or_secret.stat().st_mode
        if mode & (stat.S_IRGRP | stat.S_IROTH):
            msg = f'{path_or_secret} must be exclusively readable by the owner.'
            raise OSError(msg)
        return path_or_secret.read_text().strip()


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--install-prefix', default='/usr/local',
        type=lambda p: pathlib.Path(p).expanduser(),
        help='''Installation destination. Scripts will be copies into the bin
                subdirectory here.''')
    parser.add_argument(
        '--gitlab-url', default='https://gitlab.com',
        help='''GitLab server URL.''')
    parser.add_argument(
        '--registration-token', default='~/.gitlab-registration-token',
        type=secret,
        help='''Runner registration token or file containing it. The file must
                be exclusively readable by the owner.''')
    parser.add_argument(
        '--auth-token', default='~/.gitlab-auth-token', type=secret,
        help='''GitLab authentication token or file containing it. The file
                must be exclusively readable by the owner.''')
    parser.add_argument(
        '--config-file', default='~/gitlab-runner-config.toml',
        type=lambda p: pathlib.Path(p).expanduser(),
        help='''Runner configuration file to use.''')
    parser.add_argument(
        '--mode', choices=('production', 'staging'), default='production',
        help='''Production level to set the executor.''')
    parser.add_argument(
        '--paused', action='store_true',
        help='''Put the runner into a paused state.''')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    install_podman_executor(args.install_prefix)
    register_podman_executor(
        args.install_prefix, args.gitlab_url, args.auth_token,
        args.registration_token, args.config_file, args.mode,
        args.paused)
